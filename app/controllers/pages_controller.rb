# frozen_string_literal: true

require 'httparty'

class PagesController < ApplicationController
  before_action :client_location?

  def client_location?
    @ip = request.ip
    @country = request.location.country
    @city = request.location.city
    url = 'https://api.openweathermap.org/data/2.5/weather?lat=' + request.location.latitude.to_s + '&lon=' + request.location.longitude.to_s + '&appid=' + ENV['WEATHER_API_KEY'] + '&units=metric'
    response = HTTParty.get(url).parsed_response
    des = response
    @c_des = des.to_s.gsub!(/.*?(?="description")/im, '')[16..].capitalize[/[^"]+/]
    temp = response
    @c_temp = temp.to_s.gsub!(/.*?(?="temp")/im, '')[8..][/[^,]+/] + ' °C'
  end

  def about; end
end
